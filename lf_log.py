"""
DEBUG:      Detailed information, typically of interest only when diagnosing problems.

INFO:       Confirmation that things are working as expected.

WARNING:    An indication that something unexpected happened,
            or indicative of some problem in the near future
            (e.g. ‘disk space low’). The software is still working as expected.

ERROR:      Due to a more serious problem, the software has not been able to perform some function.

CRITICAL:   A serious error, indicating that the program itself may be unable to continue running.

"""

import inspect
import logging
from enum import Enum
import os


def _sc():
    return '/' if os.name == 'posix' else '\\'


class Level(Enum):
    INFO = 0
    DEBUG = 1
    WARNING = 2
    CRITICAL = 3
    ERROR = 4


_LOG_LEVEL = {0: logging.INFO,
              1: logging.DEBUG,
              2: logging.WARNING,
              3: logging.CRITICAL,
              4: logging.ERROR}

_FORMAT = "{'level':%(levelname)s,'time': %(asctime)s,'msg': %(message)s} "
formatter = logging.Formatter(_FORMAT)
file_formatter=logging.Formatter(
    "{'time':'%(asctime)s', 'name': '%(name)s', 'level': '%(levelname)s', 'message': '%(message)s'}"
)

class Singleton(type):
    """
    Define an Instance operation that lets clients access its unique
    instance.
    """

    def __init__(cls, name, bases, attrs, **kwargs):
        super().__init__(name, bases, attrs)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance


def _setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""

    handler = logging.FileHandler(log_file)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(file_formatter)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(stream_handler)
    logger.addHandler(handler)

    return logger


class Logger(metaclass=Singleton):

    def __init__(self, file_name, level):
        if not (isinstance(file_name, str) and
                isinstance(level, int)):
            raise ValueError("Invalid Args")

        self.log_inf = _setup_logger('inf', file_name+'.inf', level)
        self.log_err = _setup_logger('err', file_name+'.err', level)

        self.msg_tbl = {Level.INFO.value: self.log_inf.info,
                        Level.DEBUG.value: self.log_inf.debug,
                        Level.ERROR.value: self.log_err.error,
                        Level.WARNING.value: self.log_inf.warning,
                        Level.CRITICAL.value: self.log_inf.critical}
    def __del__(self):
        for handler in self.log_inf.handlers:
            handler.close()
        for handler in self.log_err.handlers:
            handler.close()


    def msg(self, m_level=Level.DEBUG.value, message=None):
        callerframerecord = inspect.stack()[1]  # 0 represents this line
        # 1 represents line at caller
        frame = callerframerecord[0]
        info = inspect.getframeinfo(frame)
        print(m_level, message)
        msg_func = self.msg_tbl.get(m_level, self.log_err.error)
        msg_func("{_file} {func}() {line}:: {mess}".format(_file=info.filename.split(_sc())[-1],
                                                           func=info.function,
                                                           line=info.lineno,
                                                           mess=message))




if __name__ == '__main__':
    log = Logger("hello.log", Level.DEBUG.value)

    def chandan(arg):
        log.msg(Level.DEBUG.value, "Hello this is {}".format(123))
        log.msg(Level.ERROR.value, "Hello ERROR is {}".format(123))

    def Main():
        log.msg(Level.DEBUG.value, "Hello this is {}".format(123))
        chandan("ccndckcdln")


    Main()
