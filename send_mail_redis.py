import redis
import smtplib

from lf_log import *
import json, time

from daemon import Daemon

class MyDaemon(Daemon):
    def run(self):
        while True:
            time.sleep(1)


class RAMUEmail(object):

    def __init__(self, receivers, subject):

        if not isinstance(receivers, list) or\
                not isinstance(subject, str):
            raise ValueError("invalid args")
        self._rcvrs = receivers
        self._sub = subject
        self._sender = 'pythonre4@gmail.com'
        self._psswd = 'pythonre@1234'
        self._server = smtplib.SMTP('smtp.gmail.com', 587)
        self._PROJECT_NAME = "system_monitor"
        self.log = _get_log_module()
        self._server.ehlo()
        self._server.starttls()
        self._server.login(self._sender,\
                           self._psswd)


    def _get_body(self, msg):

        if not isinstance(msg, str):
            raise ValueError("invalid msg")

        return  '\r\n'.join(['To: %s' % self._rcvrs,
                        'From: %s' % self._sender,
                        'Subject: %s' % self._sub,
                        '', msg])

    def send_mail(self, msg):
        try:
            self._server.sendmail(self._sender,\
                                  self._rcvrs,\
                                  self._get_body(msg))
            self.log.msg(Level.INFO.value, "msg {} sent".format(msg))
        except Exception as ex:
            self.log.msg(Level.ERROR.value, "error sending mail {}".format(ex))

    # def __del__(self):
    #     self._server.quit()

    def run(self):
        while True:
            time.sleep(1)

def _get_details():
    with open(os.path.join('mail_req.json')) as f:
        return json.load(f)

def _get_log_module():
    details = _get_details()
    _LOG_PATH = "/home/ubuntu/logs/" if os.name == 'posix' else "E:\\"
    return Logger(_LOG_PATH + details['PROJECT_NAME'] + ".json", level=Level.DEBUG.value)


def main():
    details = _get_details()
    s_email = RAMUEmail(details['send_mails'],
                        details['mail_subj'])

    _re_c = redis.Redis(details['redis_map']['host'],
                        details['redis_map']['port'],
                        details['redis_map']['db'])
    while True:
        msg = _re_c.blpop("email_messages", timeout=0)
        s_email.send_mail(str(msg))


if __name__ == '__main__':

    app = main()




